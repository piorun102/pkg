package storage

import (
	"context"
	"fmt"
	_ "github.com/jackc/pgx/stdlib" // pgx driver
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	_ "github.com/lib/pq"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/piorun102/lg"
	"gitlab.com/piorun102/pkg/utils"
)

type DBConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Db       string
	SslMode  string
}

var sqlFC = 2

func NewDB(ctx context.Context, cfg *DBConfig) (*pgxpool.Pool, error) {
	connectionUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.Db, cfg.SslMode)
	return pgxpool.New(ctx, connectionUrl)
}
func NewDBString(ctx context.Context, str string) (p *pgxpool.Pool, err error) {
	cfg, err := pgxpool.ParseConfig(str)
	if err != nil {
		return
	}
	return pgxpool.NewWithConfig(ctx, cfg)
}
func SelectStruct[T any](ctx lg.CtxLogger, pool *pgxpool.Pool, sql string, args ...any) (t []T, err error) {
	var rows pgx.Rows
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), dbOperation)
	sp.LogKV("sql REQ", utils.ResSql(sql, args...))
	ctx.TracefFC(sqlFC, "SQL: "+utils.ResSql(sql, args...))
	rows, err = pool.Query(ctx.Ctx(), sql, args...)
	if err != nil {
		sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
		sp.Finish()
		rows.Close()
		return
	}
	t, err = pgx.CollectRows(rows, pgx.RowToStructByName[T])
	sp.LogKV("sql RESP", fmt.Sprintf("%#v %v", t, err))
	sp.Finish()
	rows.Close()
	return
}

func SelectSimple[T any](ctx lg.CtxLogger, pool *pgxpool.Pool, sql string, args ...any) (t []T, err error) {
	var rows pgx.Rows
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), dbOperation)
	sp.LogKV("sql REQ", utils.ResSql(sql, args...))
	ctx.TracefFC(sqlFC, "SQL"+utils.ResSql(sql, args...))
	rows, err = pool.Query(ctx.Ctx(), sql, args...)
	if err != nil {
		sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
		sp.Finish()
		rows.Close()
		return
	}
	t, err = pgx.CollectRows(rows, pgx.RowTo[T])
	sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
	sp.Finish()
	rows.Close()
	return
}
func Get[T any](ctx lg.CtxLogger, pool *pgxpool.Pool, sql string, args ...any) (t T, err error) {
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), dbOperation)
	sp.LogKV("sql REQ", utils.ResSql(sql, args...))
	ctx.TracefFC(sqlFC, "SQL"+utils.ResSql(sql, args...))
	err = pool.QueryRow(ctx.Ctx(), sql, args...).Scan(&t)
	sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
	sp.Finish()
	return
}

func GetStruct[T any](ctx lg.CtxLogger, pool *pgxpool.Pool, sql string, args ...any) (t T, err error) {
	var (
		rows pgx.Rows
	)
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), dbOperation)
	sp.LogKV("sql REQ", utils.ResSql(sql, args...))
	ctx.TracefFC(sqlFC, "SQL"+utils.ResSql(sql, args...))
	rows, err = pool.Query(ctx.Ctx(), sql, args...)
	if err != nil {
		sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
		sp.Finish()
		rows.Close()
		return
	}
	t, err = pgx.CollectOneRow(rows, pgx.RowToStructByName[T])
	sp.LogKV("sql RESP", fmt.Sprintf("%v %v", t, err))
	sp.Finish()
	rows.Close()
	return
}

func Insert(ctx lg.CtxLogger, pool *pgxpool.Pool, sql string, args ...interface{}) (err error) {
	var rows pgx.Rows
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), dbOperation)
	sp.LogKV("sql REQ", utils.ResSql(sql, args...))
	ctx.TracefFC(sqlFC, "SQL"+utils.ResSql(sql, args...))
	rows, err = pool.Query(ctx.Ctx(), sql, args...)
	rows.Close()
	sp.LogKV("sql RESP ERR", fmt.Sprintf("%v", err))
	sp.Finish()
	return err
}

const dbOperation = "DB"
