package bwgNats

import (
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"gitlab.com/piorun102/lg"
	"io"
)

type SubscribeCallback func(ctx lg.CtxLogger, msg *nats.Msg) ProtoMessage

type bwgNats struct {
	nc     *nats.Conn
	closer io.Closer
	kv     jetstream.KeyValue
}

type bwgJs struct {
	stream jetstream.Stream
	cons   jetstream.Consumer
	js     jetstream.JetStream
	kv     jetstream.KeyValue
}
