package bwgNats

import (
	"context"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"gitlab.com/piorun102/lg"
	"time"
)

const cache = "cache"

func getKv(nc *nats.Conn) (jetstream.KeyValue, error) {
	js, err := jetstream.New(nc,
		jetstream.WithPublishAsyncMaxPending(256),
		jetstream.WithPublishAsyncErrHandler(func(stream jetstream.JetStream, msg *nats.Msg, err error) {
			lg.Errorf("err: %v with msg: %+v", err, msg)
		}))

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	kv, err := js.KeyValue(ctx, cache)
	if err == nil {
		return kv, err
	}
	kv, err = js.CreateKeyValue(ctx, jetstream.KeyValueConfig{
		Bucket:  cache,
		Storage: jetstream.MemoryStorage,
	})
	if err != nil {
		return nil, err
	}

	return kv, nil
}
