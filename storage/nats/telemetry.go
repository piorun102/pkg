package bwgNats

import (
	"bytes"
	"fmt"
	"gitlab.com/piorun102/lg"
	"gitlab.com/piorun102/pkg/tracing/jaeger/metrics"
	"io"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/piorun102/pkg/tracing/jaeger-client"
	jaegercfg "gitlab.com/piorun102/pkg/tracing/jaeger-client/config"
)

// TraceMsg will be used as an io.Writer and io.Reader for the span's context and
// the payload. The span will have to be written first and read first.
type TraceMsg struct {
	bytes.Buffer
}

// NewTraceMsg creates a trace msg from a NATS message's data payload.
func NewTraceMsg(m *nats.Msg) *TraceMsg {
	b := bytes.NewBuffer(m.Data)
	return &TraceMsg{*b}
}

// InitTracing handles the common tracing setup functionality, and keeps
// implementation specific (Jaeger) configuration here.

var Tracer opentracing.Tracer = nil

func InitTracing(serviceName string, url string) io.Closer {
	// Sample configuration for testing. Use constant sampling to sample every trace
	// and enable LogSpan to log every span via configured Logger.
	cfg := jaegercfg.Configuration{
		ServiceName: serviceName,
		RPCMetrics:  true,
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:          true,
			CollectorEndpoint: url,
		},
	}

	// Example logger and metrics factory. Use txHandler/pkg/tracing/jaeger-client/log
	// and txHandler/pkg/tracing/jaeger/metrics respectively to bind to real logging and metrics
	// frameworks.
	jMetricsFactory := metrics.NullFactory
	// Initialize tracer with a logger and a metrics factory
	tracer, closer, err := cfg.NewTracer(
		jaegercfg.Logger(jaeger.NullLogger),
		jaegercfg.Metrics(jMetricsFactory),
	)
	if err != nil {
		lg.Fatalf("couldn't setup tracing: %v", err)
	}
	opentracing.SetGlobalTracer(tracer)
	Tracer = tracer
	return closer
}

const (
	reconnect  = "reconnect"
	closed     = "closed"
	disconnect = "disconnect"
)

// setupConnOptions sets up connection options with a tracer to trace
// salient events.
func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second
	timeout := time.Second * 10
	tracer := opentracing.GlobalTracer()

	opts = append(opts, nats.Timeout(timeout))
	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
		span := tracer.StartSpan("Disconnect Handler")
		msg := fmt.Sprintf("Disconnected: %v [%s]", totalWait.Minutes(), nc.ConnectedUrl())
		span.LogKV(disconnect, msg)
		// lg.Warnf(msg)
		span.Finish()
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		span := tracer.StartSpan("Reconnect Handler")
		msg := fmt.Sprintf("Reconnected [%s]", nc.ConnectedUrl())
		span.LogKV(reconnect, msg)
		// lg.Warnf(msg)
		span.Finish()
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		span := tracer.StartSpan("Closed Handler")
		msg := fmt.Sprintf("Exiting, no servers available [%s]", nc.ConnectedUrl())
		span.LogKV(closed, msg)
		// lg.Warnf(msg)
		span.Finish()

	}))
	return opts
}
