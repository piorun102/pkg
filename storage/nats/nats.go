package bwgNats

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/piorun102/lg"
	"gitlab.com/piorun102/pkg/utils"
	"google.golang.org/protobuf/proto"
	"time"
)

func New(url, jaegerUrl string) (NC, error) {
	opts := setupConnOptions([]nats.Option{})
	nc, err := nats.Connect(url, opts...)
	if err != nil {
		return nil, err
	}

	kv, err := getKv(nc)
	if err != nil {
		return nil, err
	}

	bwgN := &bwgNats{
		nc:     nc,
		closer: InitTracing(utils.GetServiceName(), jaegerUrl),
		kv:     kv,
	}

	return bwgN, err
}

func (n *bwgNats) Subscribe(topic string, fn SubscribeCallback) error {
	_, err := n.nc.Subscribe(topic, func(msg *nats.Msg) {
		var (
			span   opentracing.Span
			tracer = opentracing.GlobalTracer()
		)
		if msg.Header == nil {
			msg.Header = map[string][]string{}
		}
		sc, err := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(msg.Header))
		if err != nil && err != opentracing.ErrSpanContextNotFound {
			lg.Warnf("Warning with extracting: %v", err)
		}
		span = tracer.StartSpan(topic, opentracing.ChildOf(sc))
		defer span.Finish()

		ctx := lg.Ctx(opentracing.ContextWithSpan(context.Background(), span), msg.Header)
		defer ctx.Send()

		resp := fn(ctx, msg)
		ctx.SpanLog("RESP "+topic, resp.String())
		ctx.Tracef("RESP %v\n%v", topic, resp.String())

		msg.Data, err = proto.Marshal(resp)
		if err != nil {
			ctx.Errorf(err.Error())
			return
		}
		ext.MessageBusDestination.Set(span, topic)
		if err = opentracing.GlobalTracer().Inject(span.Context(),
			opentracing.HTTPHeaders,
			opentracing.HTTPHeadersCarrier(msg.Header)); err != nil {
			ctx.Errorf("Error with injection: %v", err)
		}
		if err := msg.RespondMsg(msg); err != nil {
			ctx.Error(err)
			return
		}
	})

	return err
}

func (n *bwgNats) Redirect(topicFrom, topicTo string) error {
	_, err := n.nc.Subscribe(topicFrom, func(msg *nats.Msg) {
		var (
			span   opentracing.Span
			tracer = opentracing.GlobalTracer()
		)

		sc, err := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(msg.Header))
		if err != nil && err != opentracing.ErrSpanContextNotFound {
			lg.Warnf("Warning with extracting: %v", err)
		}
		span = tracer.StartSpan(topicFrom, opentracing.ChildOf(sc))
		defer span.Finish()

		ctx := lg.Ctx(opentracing.ContextWithSpan(context.Background(), span), msg.Header)
		defer ctx.Send()

		msgTo := nats.NewMsg(topicTo)
		tmp := make([]byte, len(msg.Data))
		copy(tmp, msg.Data)
		msgTo.Data = tmp

		ext.MessageBusDestination.Set(span, topicFrom)
		if err = tracer.Inject(span.Context(),
			opentracing.HTTPHeaders,
			opentracing.HTTPHeadersCarrier(msgTo.Header)); err != nil {
			ctx.Errorf("Unable to inject span into redirected message: %v", err)
		}

		ctxTimeout, cancel := context.WithTimeout(context.Background(), 20*time.Second)
		defer cancel()
		ctx.Tracef("REDIRECT %s->%s", topicFrom, topicTo)
		ctx.SpanLog("REDIRECT", "%s->%s", topicFrom, topicTo)
		msgResp, err := n.nc.RequestMsgWithContext(ctxTimeout, msgTo)
		if err != nil {
			ctx.Errorf("Unable to redirect message: %v", err)
			msgResp = &nats.Msg{Header: map[string][]string{}, Subject: topicFrom, Data: make([]byte, 0)}
			if err = tracer.Inject(span.Context(),
				opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(msgResp.Header)); err != nil {
				span.LogKV("Inject "+topicFrom, fmt.Sprint(err))
				ctx.Errorf("Unable to inject span into redirected message: %v", err)
			}
		}

		if err = msg.RespondMsg(msgResp); err != nil {
			span.LogKV("RESP "+topicFrom, fmt.Sprint(err))
			ctx.Error(err)
			return
		}
	})

	return err
}

func (n *bwgNats) Request(ctx lg.CtxLogger, topic string, input, output ProtoMessage) error {
	var (
		data   []byte
		reqMsg = nats.NewMsg(topic)
	)

	reqSpan, _ := opentracing.StartSpanFromContext(ctx.Ctx(), topic)
	defer reqSpan.Finish()

	data, err := proto.Marshal(input)
	if err != nil {
		return err
	}
	reqMsg.Data = data

	ctxTimeout, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	ext.MessageBusDestination.Set(reqSpan, topic)
	if err = opentracing.GlobalTracer().Inject(reqSpan.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(reqMsg.Header)); err != nil {
		lg.Errorf("Unable to inject span: %v", err)
	}

	reqSpan.LogKV("REQ "+topic, input.String())
	ctx.Tracef("REQ %v\n%v", topic, input.String())
	msg, err := n.nc.RequestMsgWithContext(ctxTimeout, reqMsg)
	if err != nil {
		reqSpan.LogKV("RESP "+topic, fmt.Sprint(err))
		ctx.Errorf("Unable to do request: %v", err)
		return err
	}

	if err = proto.Unmarshal(msg.Data, output); err != nil {
		reqSpan.LogKV("RESP "+topic, fmt.Sprint(err))
		ctx.Error(err)
		return err
	}
	reqSpan.LogKV("RESP "+topic, output.String())
	ctx.Tracef("RESP %v\n%v", topic, output.String())
	return nil
}

func (n *bwgNats) Drain() error {
	if err := n.nc.Drain(); err != nil {
		lg.Errorf("Unable to drain: %v", err)
	}
	if err := n.closer.Close(); err != nil {
		lg.Errorf("Unable to close jaeger: %v", err)
		return err
	}

	return nil
}

func (n *bwgNats) InsertCache(ctx lg.CtxLogger, key string, input ProtoMessage) error {
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), cacheOperation)
	sp.LogKV(cacheOperation, key)
	defer sp.Finish()
	data, err := proto.Marshal(input)
	if err != nil {
		return err
	}
	_, err = n.kv.Put(ctx, key, data)
	return err
}

func (n *bwgNats) SelectCache(ctx lg.CtxLogger, key string, output ProtoMessage) error {
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), cacheOperation)
	sp.LogKV(cacheOperation, key)
	defer sp.Finish()
	val, err := n.kv.Get(ctx, key)
	if err != nil {
		return err
	}
	err = proto.Unmarshal(val.Value(), output)
	return err
}

const cacheOperation = "cache"

func (n *bwgNats) InsertCacheJSON(ctx lg.CtxLogger, key string, input any) error {
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), cacheOperation)
	sp.LogKV(cacheOperation, key)
	defer sp.Finish()
	data, err := json.Marshal(input)
	if err != nil {
		return err
	}
	_, err = n.kv.Put(ctx, "JSON"+key, data)
	return err
}

func (n *bwgNats) SelectCacheJSON(ctx lg.CtxLogger, key string, output any) error {
	sp, _ := opentracing.StartSpanFromContext(ctx.Ctx(), cacheOperation)
	sp.LogKV(cacheOperation, key)
	defer sp.Finish()
	val, err := n.kv.Get(ctx, "JSON"+key)
	if err != nil {
		return err
	}
	err = json.Unmarshal(val.Value(), output)
	return err
}
