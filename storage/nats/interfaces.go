package bwgNats

import (
	"gitlab.com/piorun102/lg"
	"google.golang.org/protobuf/proto"
)

type NC interface {
	Request(ctx lg.CtxLogger, topic string, input, output ProtoMessage) error
	Subscribe(topic string, fn SubscribeCallback) error
	Redirect(topicFrom, topicTo string) error
	Drain() (err error)

	InsertCache(ctx lg.CtxLogger, key string, input ProtoMessage) error
	SelectCache(ctx lg.CtxLogger, key string, output ProtoMessage) error
	InsertCacheJSON(ctx lg.CtxLogger, key string, input any) error
	SelectCacheJSON(ctx lg.CtxLogger, key string, output any) error
}
type ProtoMessage interface {
	proto.Message
	String() string
}
