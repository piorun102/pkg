package utils

import (
	"golang.org/x/mod/modfile"
	"log"
	"os"
	"runtime"
	"sync"
)

func CurrentFunction() string {

	counter, _, _, _ := runtime.Caller(2)
	return runtime.FuncForPC(counter).Name()
}

var serviceName string
var once sync.Once

func GetServiceName() string {
	once.Do(func() {
		goModPath := "go.mod"

		content, err := os.ReadFile(goModPath)
		if err != nil {
			log.Println("Unable to read go.mod: ", err)
			serviceName = "empty"
			return
		}

		modFile, err := modfile.Parse("go.mod", content, nil)
		if err != nil {
			log.Println("Unable to parse go.mod: ", err)
			serviceName = "empty"
			return
		}

		serviceName = modFile.Module.Mod.Path
	})

	return serviceName
}
