package utils

import (
	"fmt"
	"github.com/shopspring/decimal"
)

func GreaterOfEqual(excepted, real decimal.Decimal) error {
	if !real.GreaterThanOrEqual(excepted) {
		return fmt.Errorf("insufficient balance, got %v, but need %v, need %v more", excepted, real, excepted.Sub(real))
	}
	return nil
}
func Greater(excepted, real decimal.Decimal) error {
	if !real.GreaterThan(excepted) {
		return fmt.Errorf("insufficient balance, got %v, but need %v, need %v more", excepted, real, excepted.Sub(real))
	}
	return nil
}
func Equal(excepted, real decimal.Decimal) error {
	if !real.Equal(excepted) {
		return fmt.Errorf("not equal balance, got %v, but need %v", excepted, real)
	}
	return nil
}
func LessThanOrEqual(excepted, real decimal.Decimal) error {
	if !real.LessThanOrEqual(excepted) {
		return fmt.Errorf("not less or equal balance, got %v, but need %v", excepted, real)
	}
	return nil
}
