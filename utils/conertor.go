package utils

import (
	"github.com/shopspring/decimal"
	"google.golang.org/protobuf/types/known/timestamppb"
	"time"
)

func TimestampToTime(ts *timestamppb.Timestamp) time.Time {
	if ts == nil {
		return time.Time{}
	}
	return time.Unix(ts.Seconds, int64(ts.Nanos))
}
func TimestampToTimeNil(ts *timestamppb.Timestamp) *time.Time {
	if ts == nil {
		return &time.Time{}
	}
	t := time.Unix(ts.Seconds, int64(ts.Nanos))
	return &t
}

func StringToInt(s string) (int, error) {
	dec, err := decimal.NewFromString(s)
	if err != nil {
		return 0, err
	}
	result := dec.Mul(decimal.New(1, USDT_DECIMALS)).IntPart()

	return int(result), nil
}
func IntToString(i int64) (string, error) {
	dec := decimal.NewFromInt(i)
	result := dec.Div(decimal.New(1, USDT_DECIMALS)).String()

	return result, nil
}

const USDT_DECIMALS = 4
