package utils

func PtrInt64(num int64) *int64 {
	return &num
}

func PtrInt32(num int32) *int32 {
	return &num
}

func PtrFloat64(num float64) *float64 {
	return &num
}

func PtrFloat32(num float32) *float32 {
	return &num
}

func PtrString(str string) *string {
	return &str
}
