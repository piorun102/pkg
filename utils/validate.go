package utils

import (
	"fmt"
	"github.com/go-playground/validator/v10"
)

// Use a single instance of Validate, it caches struct info
var validate *validator.Validate

func init() {
	validate = validator.New(validator.WithRequiredStructEnabled())
}
func Validate(input any) error {
	return validate.Struct(input)
}
func NotNil(args ...any) (err error) {
	for _, arg := range args {
		if arg == nil {
			err = fmt.Errorf("nil value")
			return
		}
	}
	return
}
